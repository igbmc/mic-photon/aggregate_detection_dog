# Aggregates detection using a Difference of Gaussian (DoG) algorithm

## Goal

Detect and count number of clusters in fluorence image of plasma membrane in individual cells.

## Cellpose segmentation

If you want to extract the aggregates information per cell, you will need a segmentation label image for each image you want to analyze first. [Cellpose](https://cellpose.readthedocs.io/) is one of the best generically good models to segment most image that have a clear membrane stain. Follow their documentation to see how to [install it](https://github.com/MouseLand/cellpose#instructions) and [use it](https://cellpose.readthedocs.io/en/latest/command.html) on your computer.

### Quick Cellpose explanation:

- [Install miniconda](https://docs.conda.io/en/main/miniconda.html):
- Open conda terminal
- Use following commands to create environment and install cellpose

```bash
$ conda create --name cellpose python=3.8
$ conda activate cellpose
$ pip install cellpose
```

- Use cellpose on a folder of images (should only contain the images you want to segment)

```bash
$ python -m cellpose --dir ~/path/to/images --pretrained_model cyto2 --diameter 200 --save_tif --no_npy --verbose
```

## Analysis

- `batch_dog_analysis.ijm`: This ImageJ macro implements a [simple DoG algorithm](https://en.wikipedia.org/wiki/Difference_of_Gaussians) to detect the fluorescent aggregates in each image of the input directory. The macro saves the detected aggregates as a ROI Manager roi (multi point), as an overlay image with the original image for visualization and as a thresholded binary image (possible useful to measure total aggregate area and other morphometrics).
- `test_aggregates_detection_method.ijm`: this is a test macro you can use to fine-tune parameters and select the best approach for the data. You can either use a pure DoG analysis where you fine-tune the parameters, or estimate the background value from background negative images and use the estimated threshold to find maxima in the positive test images.
- `batch_aggregates_per_cell.ijm`: this is the batch version of the macro above. Use the fine-tuned approach from before in this macro, then the code will process all images in the desired folder and will also save to disk the binary images and Imagej ROIs of the detected spots per cell.

## Instructions

- Segment your cells with cellpose following [the instructions above](#cellpose-segmentation)
- Download Fiji [here](https://imagej.net/software/fiji/downloads)
- Open the corresponding macro file by drag and dropping it into the main Fiji window
- Read provided instructions ontop of the macro file
- Press Run and follow the prompts to select input and output folders

## Author

Marco Dalla Vecchia dallavem@igbmc.fr
