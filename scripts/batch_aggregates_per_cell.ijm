// ******************************************
// INSTRUCTIONS

// REQUIREMENTS:
// - Pre-computed cell segmentation label images --> use cellpose to get these
// --> these images have to have the same name as the original image followed by "_cp_masks".
// - MorphoLibJ installed in your Fiji installation --> https://imagej.net/plugins/morpholibj

// Batch process all images in defined folder
// You can use two approaches:
// 1. Use pure DoG algorithm with fine-tuned params: simply run the DoG() function after opening the desired image in Fiji
// 2. Use the background images to estimate a minimum prominence level to apply to open image: use the estimateThresholdValues() function followed by the findMaxima() function

// Use the test test_aggregates_detection_method.ijm macro on a few images to setup the aggregates segmentation first, then use the same here where we process all the images together

// In addition to find the aggregates, this macro will also split those spots into each detected cell in the segmentation mask.
// ******************************************

// AUTHOR
// Marco Dalla Vecchia dallavem@igmbc.fr

// Define Input and Output folders
dataDir = getDir("Source folder with spots and segmentation images");
bgDir = getDir("Source folder with background images");
outputDir = getDir("Source folder for saving the output");

// If you want to use this approach, estimate the low value as prominence
lowerThreshold = estimateThresholdValues(bgDir, "ni", "Intermodes");

// Use this function to build an array containing a correspodance between raw images and the segmentation mask
// the images are ordered 2 by 2: raw image - label image, raw image - label image, raw image - label image etc..
imageNamesArray = buildNamesArray(dataDir, "doxy");

// Loop through all images in the built array
for (imgCounter = 0; imgCounter < imageNamesArray.length; imgCounter += 2) { // we have a step of 2 to go through each pair
	// Open the spots image
	open(dataDir + imageNamesArray[imgCounter]); // first we have raw image
	imgWithSpotsName = getTitle();
	// Open the  mask image
	open(dataDir + imageNamesArray[imgCounter + 1]); // immediately after we have the label image
	labelsName = getTitle();
	
	// Compute spots per cells
	computeSpotsPerCell(imgWithSpotsName, labelsName, lowerThreshold);
}


// ******************************************
// FUNCTIONS DEFINITION
// ******************************************
function computeSpotsPerCell(imgWithSpotsName, labelsName, lowerThreshold) {
	// OPTIONAL: remove labels that are not good -> touching borders? Too small?
	run("Label Size Filtering", "operation=Greater_Than size=20000");	// adjust size parameter if needed
	filteredLabelsName = getTitle();
	close(labelsName);
	
	// Find spots using the low threshold from background image..
	findMaxima(imgWithSpotsName, lowerThreshold);
	
	// ..Or via DoG alone
	// Adjust sigmas to improve the dots detection
//	low_sigma = 1;
//	high_sigma = 5;
//	prominence = 100;
//	
//	DoG(imgWithSpotsName, low_sigma, high_sigma, prominence);
	
	// Now that you have a spot image, create an image for each cell label separately and save it to outputDir
	spotsImageName = getTitle();
	filterSpotsByLabel(spotsImageName, filteredLabelsName, outputDir);	
	
	// Close images we don't need anymore
	close(imgWithSpotsName);
	close(spotsImageName);
	close(filteredLabelsName);
}

function buildNamesArray(dataDir, filterString) {
	// Get full list of files present in input folder
	fileList = getFileList(dataDir);
	
	namesArray = newArray();
	
	// Loop through all files
	for (i = 0; i < fileList.length; i++) {
		// If it contains filterString and 561 and is a TIF then add it to array
		if (indexOf(fileList[i], filterString) >= 0 && indexOf(fileList[i], "561") >= 0 && fileList[i].endsWith(".TIF")) {
			namesArray = Array.concat(namesArray, fileList[i]);
		}
		// If it contains "_cp_masks" and is also a tif then add it to array
		if (indexOf(fileList[i], filterString) >= 0 && indexOf(fileList[i], "_cp_masks") >= 0 && fileList[i].endsWith(".tif")) {
			namesArray = Array.concat(namesArray, fileList[i]);
		}
	}
	// return the array which has raw image followed up by label image in order
	return namesArray;
}

function filterSpotsByLabel(spotsImageName, labelsName, outputDir) { 
	// This function takes a binary spots image and a label segmentation image and
	// generates a separate image with corresponding spots for each cell label
	// and saves it into outputDir folder
	
	selectImage(labelsName);
	// Get statistics to get the max number of labels present in the label image
	getStatistics(area, mean, min, max, std, histogram);
	
	// Loop through all labels in segmentation image
	for (lblId = 1; lblId <= max; lblId++) {
		selectImage(labelsName);
		// Select one label using MorphoLibJ
		run("Select Label(s)", "label(s)=" + lblId);
		currentLabelName = getTitle();
		// Convert the selected label image to a binary image (0 background, 255 where the cell is)
		setThreshold(1, 255);
		run("Convert to Mask");
		
		// Create a copy of the spots image to do multiplication on
		selectImage(spotsImageName);
		run("Duplicate...", "title=spots_label" + lblId);
		// Multiply the binary cell image with the spots image
		// when a dot is in the background is multiplied by 0 and get 'deleted'
		imageCalculator("Multiply", "spots_label" + lblId, currentLabelName);
		
		// Not all cell have spots so..
		selectImage("spots_label" + lblId);
		// ..check the max value of the multiplied image
		getStatistics(area, mean, min, max, std, histogram);
		// if max > 0 it means that there are spots left after the multiplication
		if (max > 0) {
			// Select all the spots as a ROI
			run("Find Maxima...", "prominence=0 output=[Point Selection]");
			roiManager("add");
			// And save it to disk
			roiManager("Save", outputDir + File.getNameWithoutExtension(spotsImageName) +  "_spots_label" + lblId + ".roi");
			roiManager("reset");
			run("Select None");
		}		
		// Export image with the spots for this cell
		selectImage("spots_label" + lblId);
		saveAs("Tiff", outputDir + File.getNameWithoutExtension(spotsImageName) +  "_spots_label" + lblId + ".tif");
		// Close the images before starting the next loop
		close(File.getNameWithoutExtension(spotsImageName) +  "_spots_label" + lblId + ".tif");
		close(currentLabelName);
	}
}


function estimateThresholdValues(dataDir, bgString, method) {
	// this function asks for a bgString to identify all background images in the provided dataDir folder
	// To each background image, it applied an automatic threshold method selected by the method string. Select a method that excludes most of the background
	// It then calculates the mean of the low threshold values of all images and returns it
	
	fileList = getFileList(dataDir);
	
	lowValues = newArray();
	highValues = newArray();
	
	// Go through all background images in the folder
	for (i = 0; i < fileList.length; i++) {
		// select images that contain bgString, and that have 561 in the name and that ends in TIF
		if (indexOf(fileList[i], bgString) >= 0 && indexOf(fileList[i], "561") >= 0  && fileList[i].endsWith(".TIF")) {
			open(dataDir + fileList[i]);
			imgName = getTitle();
			selectImage(imgName);
			// Apply threshold method
			setAutoThreshold(method + " dark");
			// Get low and high threshold values
			getThreshold(lower, upper);
			// Remembers them in these arrays
			lowValues = Array.concat(lowValues, lower);
			highValues = Array.concat(highValues, upper); // we don't really use this but it's here for completeness
			close(imgName);
		}
	}
	// Calculate mean values of the low thresholds
	Array.getStatistics(lowValues, min, max, mean, stdDev);
	return mean; // return mean value of the low threshold of all background images
}

function findMaxima(imgName, prominence) { 
	// Find maxima from image with given prominence and renames output image using the input image name
	
	selectImage(imgName);
	run("Find Maxima...", "prominence=" + prominence + " output=[Single Points]");
	rename(imgName + "_spots");
}

function DoG(imgName, low_sigma, high_sigma, prominence) { 
	// This function performs the difference of gaussian algorithm based on provided low and high sigma
	// and detects maxima on the DoG image based on provided prominence value
	

	selectImage(imgName);
	// create two images: one for the low blur and one for the high blur
	run("Duplicate...", "title=gaussian_low");
	selectImage(imgName);
	run("Duplicate...", "title=gaussian_high");
	
	// Blur the images according to the sigmas defined above
	selectImage("gaussian_low");
	run("Gaussian Blur...", "sigma=" + low_sigma);
	selectImage("gaussian_high");
	run("Gaussian Blur...", "sigma=" + high_sigma);
	
	// Subtract the two blurred images to implement the DoG algorithm (Difference of Gaussians)
	imageCalculator("Subtract create", "gaussian_low","gaussian_high");
	rename(imgName + "_DoG");
	dog_img = getTitle();
	
	// Get maxima of the DoG image to count points
	findMaxima(dog_img, prominence);
	close("gaussian_low");
	close("gaussian_high");
//	close(imgName);
	close(dog_img);
}
