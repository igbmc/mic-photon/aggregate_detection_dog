// ******************************************
// INSTRUCTIONS

// Open an image before using this code
// Test with a couple of images the parameters below the aggregates detection

// You can use two approaches:
// 1. Use pure DoG algorithm with fine-tuned params: simply run the DoG() function after opening the desired image in Fiji
// 2. Use the background images to estimate a minimum prominence level to apply to open image: use the estimateThresholdValues() function followed by the findMaxima() function

// ******************************************

// AUTHOR
// Marco Dalla Vecchia dallavem@igmbc.fr


// Adjust sigmas to improve the dots detection
low_sigma = 1;
high_sigma = 5;
prominence = 100;

// Open image and get title here
imgNameReal = getTitle();



DoG(imgNameReal, low_sigma, high_sigma, prominence);


//lowerThreshold = estimateThresholdValues("ni", "Intermodes");
//findMaxima(imgNameReal, lowerThreshold);

// ******************************************
// FUNCTIONS DEFINITION
// ******************************************
function estimateThresholdValues(bgString, method) {
	// this function asks for a directory and a bgString to identify all background images
	// To each background image, it applied an automatic threshold method selected by the method string. Select a method that excludes most of the background
	// It then calculates the mean of the low threshold values of all images and returns it
	
	dataDir = getDir("Source folder with background images");
	fileList = getFileList(dataDir);
	
	lowValues = newArray();
	highValues = newArray();
	
	// Go through all background images in the folder
	for (i = 0; i < fileList.length; i++) {
		// select images that contain bgString, and that have 561 in the name and that ends in TIF
		if (indexOf(fileList[i], bgString) >= 0 && indexOf(fileList[i], "561") >= 0  && fileList[i].endsWith(".TIF")) {
			open(dataDir + fileList[i]);
			imgName = getTitle();
			selectImage(imgName);
			// Apply threshold method
			setAutoThreshold(method + " dark");
			// Get low and high threshold values
			getThreshold(lower, upper);
			// Remembers them in these arrays
			lowValues = Array.concat(lowValues, lower);
			highValues = Array.concat(highValues, upper); // we don't really use this but it's here for completeness
			close(imgName);
		}
	}
	// Calculate mean values of the low thresholds
	Array.getStatistics(lowValues, min, max, mean, stdDev);
	return mean; // return mean value of the low threshold of all background images
}

function findMaxima(imgName, prominence) { 
	// Find maxima from image with given prominence and renames output image using the input image name
	
	selectImage(imgName);
	run("Find Maxima...", "prominence=" + prominence + " output=[Single Points]");
	rename(imgName + "_spots");
}

function DoG(imgName, low_sigma, high_sigma, prominence) { 
	// This function performs the difference of gaussian algorithm based on provided low and high sigma
	// and detects maxima on the DoG image based on provided prominence value
	

	selectImage(imgName);
	// create two images: one for the low blur and one for the high blur
	run("Duplicate...", "title=gaussian_low");
	selectImage(imgName);
	run("Duplicate...", "title=gaussian_high");
	
	// Blur the images according to the sigmas defined above
	selectImage("gaussian_low");
	run("Gaussian Blur...", "sigma=" + low_sigma);
	selectImage("gaussian_high");
	run("Gaussian Blur...", "sigma=" + high_sigma);
	
	// Subtract the two blurred images to implement the DoG algorithm (Difference of Gaussians)
	imageCalculator("Subtract create", "gaussian_low","gaussian_high");
	rename(imgName + "_DoG");
	dog_img = getTitle();
	
	// Get maxima of the DoG image to count points
	findMaxima(dog_img, prominence);
	close("gaussian_low");
	close("gaussian_high");
//	close(imgName);
	close(dog_img);
}

function estimateBackground(bgString) { 
	// Extra function not used here. This function can be useful to open all background images in selected folder which contain bgString in filename
	// and estimate a mean background value. For example you could use it estimate the mean/min/max value of each background image.
	// Estimating the STD of background images can also be useful to determine the contrast.
	
	dataDir = getDir("Source folder with background images");
	fileList = getFileList(dataDir);
	
	bgImgStats = newArray();
	
	// Go through all background images in the folder
	for (i = 0; i < fileList.length; i++) {
		// select images that contain bgString, and that have 561 in the name and that ends in TIF
		if (indexOf(fileList[i], bgString) >= 0 && indexOf(fileList[i], "561") >= 0  && fileList[i].endsWith(".TIF")) {
			open(dataDir + fileList[i]);
			imgName = getTitle();
			
			// Get statistics  from each image
			selectImage(imgName);
			getStatistics(area, mean, min, max, std, histogram);
			// and memorize it in the array
			bgImgStats = Array.concat(bgImgStats, std); // min, max, mean // append what you need!
			
			close(imgName);
		}
	}
	// calculate the mean of all values
	Array.getStatistics(bgImgStats, min, max, mean, stdDev);
	return mean // and return it
}	
// ******************************************
