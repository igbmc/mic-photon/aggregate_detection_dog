// AUTHOR
// Marco Dalla Vecchia dallavem@igmbc.fr

// Select folder with the images to analyze
dataDir = getDir("Source folder with images");
// Select folder where we save the output of the analysis
outputDir = getDir("Destination folder for output");

// Adjust sigmas to improve the dots detection
low_sigma = 1;
high_sigma = 5;

// We go through all files in the folder
fileList = getFileList(dataDir);
for (i = 0; i < fileList.length; i++) {
	close("*");
	close("ROI Manager");
	// open the file only if it's a tif file
	if (indexOf(fileList[i], "561") >= 0 && fileList[i].endsWith(".TIF")) {
		// open it and remember name
		open(dataDir + fileList[i]);
		orig_img = getTitle();
		// create two images: one for the low blur and one for the high blur
		run("Duplicate...", "title=gaussian_low");
		run("Duplicate...", "title=gaussian_high");
		
		// Blur the images according to the sigmas defined above
		selectImage("gaussian_low");
		run("Gaussian Blur...", "sigma=" + low_sigma);
		selectImage("gaussian_high");
		run("Gaussian Blur...", "sigma=" + high_sigma);
		
		// Subtract the two blurred images to implement the DoG algorithm (Difference of Gaussians)
		imageCalculator("Subtract create", "gaussian_low","gaussian_high");
		dog_img = getTitle();
		// Create a copy of the DoG image
		run("Duplicate...", "title=dog_thresholded");
		// Use automatic thresholding to create binary mask of all aggregates
		setAutoThreshold("Huang dark no-reset");
		setOption("BlackBackground", true);
		run("Convert to Mask");
		// Save binary image and close it
		saveAs("Tiff", outputDir + File.getNameWithoutExtension(orig_img) + "_thresholded.tif");
		close(File.getNameWithoutExtension(orig_img) + "_thresholded.tif");
		
		// Also use a Find Maxima algorithm to get the number of aggregates
		// Here we do it twice to save both an image and the points as a ROI
		selectImage(dog_img);
		run("Find Maxima...", "prominence=100 output=[Single Points]"); // here we create an image
		run("16-bit");
		maxima_img = getTitle();
		// Create an overlay of the original image with the detected dots for visualization
		run("Merge Channels...", "c1=[" + maxima_img + "] c2=" + orig_img + " create keep");
		// Save it and close it
		saveAs("Tiff", outputDir + File.getNameWithoutExtension(orig_img) + "_composite.tif");
		close(File.getNameWithoutExtension(orig_img) + "_composite.tif");
		
		selectImage(dog_img);
		run("Find Maxima...", "prominence=100 output=[Point Selection]"); // here we create points ROI
		roiManager("Add");
		selectImage(orig_img);
		// Save the points to disk
		roiManager("Save", outputDir + File.getNameWithoutExtension(orig_img) + ".roi");
		
		// Close all other opened images
		close(dog_img);
		close(maxima_img);
		close("gaussian_low");
		close("gaussian_high");
		close(orig_img);
	}	
}

